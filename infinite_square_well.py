import numpy as np
from matplotlib import pyplot as pl
from matplotlib import animation
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

fig, ax = plt.subplots()

ln_imag, = plt.plot([], [], 'b-')
ln_real, = plt.plot([], [], 'r-')

y1 = np.array([0, 0.41, -0.02, 0.10, 0.08, 0.05, 0.01, 0.04, -0.01, 0.02, -0.01, 0.01, -0.01])
y2 = np.array([0, 0.27, 0.23, -0.13, 0.19, -0.00, -0.04, 0.02, -0.02, 0.02, -0.01, 0.01, 0.01])

# length of the box
L = 1.73

# normalization constant
A = np.power(L * (y1 @ y1 + y2 @ y2) / 2.0, -0.5)

title = ax.set_title("")

def init():
    ax.set_xlim(0, L)
    ax.set_ylim(-2, 2)
    title.set_text("")
    return (ln_imag, ln_real)

def update(frame):
    xdata = np.linspace(0, L, 2048)
    psi_imag = 0 * xdata
    psi_real = 0 * xdata
    omega_t = frame * 1.0
    title.set_text("ωt = %.3f" % omega_t)
    for n in range(1, y1.size):
        psi_imag += A * np.sin(n * xdata * np.pi / L) * \
            (y1[n] * np.cos(-n * n * omega_t) + y2[n] * np.sin(-n * n * omega_t))
        psi_real += A * np.sin(n * xdata * np.pi / L) * (
            y2[n] * np.cos(-n * n * omega_t) - y1[n] * np.sin(-n * n * omega_t))
    ln_imag.set_data(xdata, psi_imag)
    ln_real.set_data(xdata, psi_real)
    return (ln_imag, ln_real, title)

ani = FuncAnimation(fig, update, frames=np.linspace(0, 6*np.pi, 128 * 4 * 4 * 4),
                    init_func=init, blit=False)

pl.rcParams['animation.ffmpeg_path'] = '/Users/admin/ffmpeg'
FFwriter = animation.FFMpegWriter(fps=60, extra_args=['-vcodec', 'libx264'])
ani.save('basic_animation.mp4', writer=FFwriter)
                
# plt.show()
